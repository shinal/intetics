<!DOCTYPE html>
<html>
<head>
    <title>Course</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link type="text/css" rel="stylesheet" href="../public/style.css">
    <link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


</head>
<body>

<div class="container">
    <div class="form-inline">
        <textarea type="text" class="form-control" rows="1"></textarea>
        <button type="submit" class="btn-click btn btn-primary">Send</button>
    </div>
    <div class="message"></div>
    <div class="result"></div>
    <input class="data" type="hidden" value=<?= $result ?>>
    <input type="hidden" id="token" name="csrf_token" id="csrf_token" value="">
</div>


<script src="../public/main.js"></script>
</body>
</html>

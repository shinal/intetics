<?php

namespace Services;

use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;

trait Sms
{
    /**
     * Send Sms
     *
     * @return void
     * @throws ConfigurationException
     * @throws TwilioException
     */
    public function sendSms(): void
    {
        $accountSid = $_ENV['YOUR_ACCOUNT_SID'];
        $authToken = $_ENV['YOUR_AUTH_TOKEN'];

        $client = new Client($accountSid, $authToken);

        $message = $client->messages->create($_ENV['TO_SMS'],
            [
                'from' => $_ENV['FROM_SMS'],
                'body' => 'Hello, this is a test message!'
            ]
        );

        echo $message->sid;
    }
}





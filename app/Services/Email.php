<?php

namespace Services;

trait Email
{
    /**
     * Send email
     *
     * @param string $to
     * @param string $subject
     * @param string $message
     * @return void
     */
    public function sendMail(string $to, string $subject, string $message): void
    {
        $headers = "From: yourname@example.com\r\n";
        $headers .= "Reply-To: yourname@example.com\r\n";
        $headers .= "CC: another@example.com\r\n";
        $headers .= "BCC: hidden@example.com\r\n";

        if (mail($to, $subject, $message, $headers)) {
            echo "Email notification sent successfully.";
        } else {
            echo "Failed to send email notification.";
        }
    }
}
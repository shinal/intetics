<?php

namespace Models;

use Database\Db;

abstract class Model {

    protected Db $db;

    public function __construct() {
        $this->db = new Db;
        $this->db->connect();
    }

}
<?php

namespace Models;

class Main extends Model
{
    /**
     * @param string $data
     * @return void
     */
    public function insert(string $data): void
    {
        $this->db->row('INSERT INTO twits (content) VALUES (:value)', ['value' => $data]);

    }

    /**
     * @return bool|array
     */
    public function select(): bool|array
    {
        return $this->db->row('SELECT content FROM twits');
    }
}
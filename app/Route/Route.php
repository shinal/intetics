<?php
namespace Route;

class Route
{
    /**
     * Routing
     *
     * @return void
     */
    public function start(): void
    {
        $route = urldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));

        $routing = [
            "/"        => ['controller' => 'HomeController', 'action' => 'index'],
            "/set"        => ['controller' => 'HomeController', 'action' => 'set'],
            "/get"        => ['controller' => 'HomeController', 'action' => 'get'],
        ];
        if (isset($routing[$route])) {
            $controller = 'Controllers\\' . $routing[$route]['controller'];
            $controller_obj = new $controller();
            $index = $routing[$route]['action'];
            $controller_obj->$index();
        }else {
            echo 'not routes';
        }
    }
}
<?php

namespace Controllers;

class Controller
{

    /**
     * View
     *
     * @param string $view
     * @param array $data
     * @return void
     */
    public function view(string $view, array $data = []): void
    {
        $result = $data;
        include './app/Views/' . $view . '.php';
    }
}


<?php

namespace Controllers;

use Models\Main as Model;
use Services\Email;
use Services\Sms;

class HomeController extends Controller
{
    use Email;
    use Sms;

    /**
     * Show data
     *
     * @return void
     */
    public function index(): void
    {
        $model = new Model();
        try {
            $this->view('View', json_encode($model->select()));
        } catch (\Exception $exception) {
            echo $exception->getMessage();
        }
    }

    /**
     * Insert data
     *
     * @return void
     */
    public function set(): void
    {
        $data = trim($_POST['value']);

        if (!empty($data)) {
            $model = new Model();
            try {
                $model->insert($data);
                $message = include './app/Views/Email/Email.php';
                $this->sendMail($_ENV['TO_EMAIL'], 'subject', $message);
                $this->sendSms();
            } catch (\Exception $exception) {
                echo $exception->getMessage();
            }
        } else {
            echo 'Content not exist';
        }
    }
}


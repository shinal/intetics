<?php

namespace Database;

use PDO;
use PDOStatement;

class Db
{
    /**
     * Connection
     *
     * @return PDO
     */
    public function connect(): PDO
    {
        $db = new PDO('mysql:host=' . $_ENV['HOST'] . ';dbname=' . $_ENV['DBNAME'] . '', $_ENV['USERNAME'], $_ENV['PASSWORD']);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $db;
    }

    /**
     * Prepare query
     *
     * @param string $sql
     * @param array $params
     * @return false|PDOStatement
     */
    private function query(string $sql, array $params = []): false|PDOStatement
    {
        $stmt = $this->connect()->prepare($sql);
        if (!empty($params)) {
            foreach ($params as $key => $val) {
                $stmt->bindValue(':' . $key, $val);
            }
        }
        $stmt->execute();

        return $stmt;
    }

    /**
     * Query
     *
     * @param string $sql
     * @param array $params
     * @return bool|array
     */
    public function row(string $sql, array $params = []): bool|array
    {
        $result = $this->query($sql, $params);

        return $result->fetchAll();
    }
}
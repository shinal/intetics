$(document).ready(function () {
    generateCSRFToken();
    $('.btn-click').on('click', function () {
        set();
    });
    get();
});

function set() {
    $('.btn-click').on('click', function () {
        let value = $('.form-control').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('#token').val()
            },
            type: 'POST',
            url: '/set',
            async: true,
            cache: false,
            data: {
                value: value
            },
            success: function (data) {
                location.reload();
            }
        });
    });
}

function get() {
    $.ajax({
        type: 'GET',
        url: '/',
        async: true,
        cache: false,
        data: {},
        success: function () {
            let obj = JSON.parse($('.data').val());
            obj.forEach(function (data) {
                $('.result').append('<p>' + data.content + '</p>');
            });
        }
    });
}

function generateCSRFToken() {
    $('#token').val(Math.random().toString(16).slice(2, 40));
}





